const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');
const userRoutes = require ('./routes/userRoutes');
const productRoutes = require ('./routes/productRoutes');

const port = 4005;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use('/users', userRoutes);
app.use('/products', productRoutes);




mongoose.connect("mongodb+srv://carlosmiguelyulo813:carlomigS813420@zuitt-bootcamp.4nsvfgb.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.log.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
});