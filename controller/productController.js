const Product = require('../models/products');
const orders = require("../models/orders")
const auth = require('../auth');


// Creating a product
module.exports.addProduct = (requestBody) => {

	let newProduct = new Product({
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price,
		quantity: requestBody.quantity
	})

	return newProduct.save().then((product, error) => {

		if(product) {
			return product
		} else {
			return false
		}
	}).catch(error => error.message);
}



// Get all active products
module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result;
	}).catch(error => error);
};


// Retrieve all product (admin only)
module.exports.getAllActiveFeaturedProducts = () => {

	if (data.isAdmin){
			return Product.find({}).then(result => {

		return result
		})
	} else {
		return "Not an Admin"
	}

}

// specific product
module.exports.getProduct = (requestParams) => {
	
	return Product.findById(requestParams.productId).then(result =>{
		return result
	})
}


// Update product info (admin only)
module.exports.updateProduct = (requestParams, requestBody) => {

	let updatedProduct = {
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price,
		quantity: requestBody.quantity
	}

		return Product.findByIdAndUpdate(requestParams.productId, updatedProduct).then((product, error) => {

			if(error) {
				return 'Update failed!'
			} else {
				return 'Updated successfully!'
			}
		})
}

// Archive a product
module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, error) => {

		if(data.payload === true) {

			result.isActive = true


			return result.save().then((archivedData, error) => {

				if (error) {

					return false;
				} else {

					return true;
				}
			})
		} else {

			return false
		}
	})
}

