const User = require('../models/users');
const Product = require('../models/products');
const Order = require('../models/orders');
const auth = require('../auth');


// Create Order
module.exports.createOrder = async (requestBody, userId) => {
	let userOrder = [];
	let total = 0;

	for(let i = 0; i < requestBody.length; i++) {
		let currentProduct = await Product.findById(requestBody[i].productId).then(result => {return result})
		let orderSubtotal = currentProduct.price*requestBody[i].orderQuantity;


		total += orderSubtotal;

		userOrder.push({
			productId: requestBody[i].productId,
			orderQuantity: requestBody[i].orderQuantity,
			subtotal: orderSubtotal
			
		})
	}

	let newOrder = new Order({
		totalAmount:total,
		userId: userId,
		orders:userOrder
	});

	return newOrder.save().then((user, error) => {
		if (error) {

			return 'Create order failed'
		} else {
			return 'Order placed';
		};
	});
};

//Retrieve orders
module.exports.retrieveAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	});
};

//Retrieve use orders
module.exports.myOrder = (userId) => {
	return Order.find({userId: userId}).then(result => {
		return result;
	})
}