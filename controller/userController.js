const User = require('../models/users');
const product = require('../models/products')
const order = require('../models/orders')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new user ({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return 'You are now registered!'
		}
	})
}

// User Log in
module.exports.loginUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if (result == false) {
			return 'Incorrect Email or Password'
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {
				return {
					message: `Welcome back!`,
					access: auth.createAccessToken(result)

				}

			} else {
				return 'Incorrect Email or Password'
			}
		}
	})
}

// 
// Find User Details
module.exports.userDetails = (requestBody) => {
	return User.findOne({_id: requestBody.id})
	.then(result => { 	
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
	.catch(error => {
		return false
	})
};



// Admin Status Change (admin only)
module.exports.statusUpdate = (data) => {

	return User.findById(data.userId).then((result, error) => {

		if(data.payload === true) {

			result.isAdmin = true

			return result.save().then((statusUpdate, error) => {

				
				if(error) {

					return false;

				
				} else {


					return 'Admin status successfully changed!';
				}
			})

		} else {

			return false
		}

	})
}


