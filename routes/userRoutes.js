const express = require('express');
const router = express.Router();
const userController = require('../controller/userController');
const orderController = require('../controller/orderController');
const auth = require("../auth");

// register route
router.post("/register", (request, response) => {
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});

// login route
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController));
});

// user details route
router.post("/details", auth.verify, (request,response) => {

	const userData = auth.decode(request.headers.authorization);

	userController.userDetails(userData).then(resultFromController => response.send(resultFromController));
});


// Admin Status Change (admin only)
router.put('/:userId/status', auth.verify, (request, response) => {

	const data = {
		userId : request.params.userId,
		payload : auth.decode(request.headers.authorization).isAdmin
	}

	userController.statusUpdate(data).then(resultFromController => response.send(resultFromController))
});

// Create Order

router.post("/purchase", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	const userId = auth.decode(request.headers.authorization).id;

	if(isAdmin) {
		response.send('Admin account is unable to proceed with order.')
	} else {
		orderController.createOrder(request.body, userId).then(resultFromController => response.send(resultFromController))
	};
});


// Retrieve all orders
router.get("/orders", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;

	if(isAdmin) {
		orderController.retrieveAllOrders().then(resultFromController => response.send(resultFromController));
	} else {
		response.send('Admin account required')
	};
});

// Retrieve user order
router.get("/myOrder", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.header.authorization).isAdmin;
	const userId = auth.decode(request.headers.authorization).id;

	if(isAdmin) {
		response.send('Admin account unable to proceed');
	} else {
		orderController.myOrder(userId).then(resultFromController => response.send(resultFromController))
	};
 

});



module.exports = router;