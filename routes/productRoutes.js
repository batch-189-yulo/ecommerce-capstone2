const express = require('express');
const router = express.Router();
const auth = require("../auth");

const productController = require('../controller/productController');

// Creating a product
router.post("/", auth.verify, (request, response) => {

		const userIsAdmin = auth.decode(request.headers.authorization).isAdmin

		if (userIsAdmin){
			productController.addProduct(request.body).then(resultFromController => response.send(resultFromController))
		} else {

			response.send('Admin function only!')
		}
})


// Get all active product
router.get("/", (request, response) => {

	productController.getAllActiveProducts().then(resultFromController => response.send(resultFromController))
});



// Route for retrieving all product (admin only)
router.get("/all", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	productController.getAllProducts(userData).then(resultFromController => response.send(resultFromController))
})


// Specific product
router.get("/:productId", (request, response) => {

	console.log(request.params)

	productController.getProduct(request.params).then(resultFromController => response.send(resultFromController))
});


// updating a product info (admin only)
router.put("/:productId", auth.verify, (request, response) => {

	productController.updateProduct(request.params, request.body).then(resultFromController => response.send(resultFromController))
})

// Archive a product
router.put('/:productId/archive', auth.verify, (request, response) => {

	const data = {
		productId: request.params.productId,
		payload: auth.decode(request.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => response.send(resultFromController))
});









module.exports = router;